var config = {
    paths: {
        'jqueryUI': 'jquery/jquery-ui',
        'popper': 'js/popper.min',
        'bootstrap': 'js/bootstrap.bundle.min'
    },
    shim: {
        'jqueryUI': {
            'deps': ['jquery']
        },
        'bootstrap': {
            'deps': ['jquery']
        }
    }
};